<?php

if ( ! class_exists( 'ICIAV_Taxonomy_Metatable' ) ) {
	/**
	 * Create table for taxonomy metadata
	 *
	 * Class ICIAV_Taxonomy_Metatable
	 *
	 * @package innocode-category-image-and-video
	 */
	class ICIAV_Taxonomy_Metatable {
		use ICIAV_Singleton;

		/** Quick touchup to wpdb */
		public function wpdbfix() {
			global $wpdb;

			$wpdb->taxonomymeta = "{$wpdb->prefix}taxonomymeta";
		}

		/**
		 * Create taxonomy metatable
		 *
		 * @param bool $blog_id
		 */
		public function create_taxonomy_metatable( $blog_id = false ) {
			global $wpdb;

			if ( $blog_id !== false ) {
				switch_to_blog( $blog_id );
			}

			$charset_collate = '';
			if ( ! empty( $wpdb->charset ) ) {
				$charset_collate = "DEFAULT CHARACTER SET $wpdb->charset";
			}
			if ( ! empty( $wpdb->collate ) ) {
				$charset_collate .= " COLLATE $wpdb->collate";
			}

			$tables = $wpdb->get_results( "show tables like '{$wpdb->prefix}taxonomymeta'" );
			if ( ! count( $tables ) ) {
				$wpdb->query( "CREATE TABLE {$wpdb->prefix}taxonomymeta (
				meta_id bigint(20) unsigned NOT NULL auto_increment,
				taxonomy_id bigint(20) unsigned NOT NULL default '0',
				meta_key varchar(255) default NULL,
				meta_value longtext,
				PRIMARY KEY	(meta_id),
				KEY taxonomy_id (taxonomy_id),
				KEY meta_key (meta_key)
			) $charset_collate;" );
			}
		}

		/**
		 * Remove taxonomy metatable
		 *
		 * @param $blog_id
		 */
		public function remove_taxonomy_metatable( $blog_id ) {
			if ( !defined( 'WP_UNINSTALL_PLUGIN' ) ){
				exit();
			}

			global $wpdb;

			if ( $blog_id !== false ) {
				switch_to_blog( $blog_id );
			}

			$wpdb->query( "DROP TABLE IF EXISTS {$wpdb->prefix}taxonomymeta" );

			unset( $wpdb->taxonomymeta );
		}

		/**
		 * Create taxonomy metatable for new blog
		 * in wpmu
		 *
		 * @param $blog_id
		 * @param $user_id
		 * @param $domain
		 * @param $path
		 * @param $site_id
		 * @param $meta
		 */
		public function new_blog( $blog_id, $user_id, $domain, $path, $site_id, $meta ) {
			if ( is_plugin_active_for_network( plugin_basename( __FILE__ ) ) ) {
				$this->create_taxonomy_metatable( $blog_id );
			}
		}
	}
}
