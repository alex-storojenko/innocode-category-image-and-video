<?php

/**
 * Source list for autoload files
 *
 * @package innocode-category-image-and-video
 */
require __DIR__ . '/iciav-capability-functions.php';
require __DIR__ . '/class-iciav-taxonomy-metatable.php';