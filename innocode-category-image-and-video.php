<?php

/*
Plugin Name: Innocode Category Image And Video
Plugin URI:
Description: Innocode Category Image And Video gives you the ability to quickly and easily add image form media library and video from Youtube/Vimeo to wordpress categories. Based on the WordPress APIs.
Version: 1.0
Author: allrain
Author URI: https://profiles.wordpress.org/allrain
License: GNU General Public License v3.0
*/

if ( ! defined( 'ABSPATH' ) ) {
	http_response_code( 403 ) && wp_die( 'Access forbidden' );
}

/** @const string version of plugin */
define( 'INNOCODE_CIAV_VERSION', '1.0' );

/** @const string plugin path */
define( 'INNOCODE_CIAV_PATH', plugin_dir_path( __FILE__ ) );

/** @const string plugin uri */
define( 'INNOCODE_CIAV_URI', plugin_dir_url( __FILE__ ) );

//Require utilities
require __DIR__ . '/utility/autoload.php';

if ( ! class_exists( 'Innocode_Category_Image_And_Video' ) ) {
	/**
	 * Plugin initializer
	 *
	 * Class Innocode_Category_Image_And_Video (ICIAV)
	 *
	 * @package innocode-category-image-and-video
	 */
	final class Innocode_Category_Image_And_Video {
		use ICIAV_Singleton;

		/**
		 * @inheritdoc
		 *
		 * Innocode_Category_Image_And_Video constructor
		 */
		protected function __construct() {
			/* Autoload components */
			require __DIR__ . '/inc/autoload.php';
		}

		/** Add image and video fields to category */
		public function exec() {
			$taxonomy = 'category';
			$inputs   = array(
				array(
					'id'                => 'category_image',
					'label'             => 'Image',
					'type'              => 'image',
					'sanitize_callback' => 'url_input'
				),
				array(
					'id'                => 'category_video',
					'label'             => 'Video Youtube/Vimeo',
					'type'              => 'video',
					'description'       => '',
					'sanitize_callback' => 'url_input'
				),
			);

			$iciav = ICIAV_Taxonomy_Metafields::create( $taxonomy, $inputs );

			add_filter( "manage_edit-{$taxonomy}_columns", function ( $columns ) {
				$columns['thumbnail'] = 'Thumbnail';

				return $columns;
			}, 10 );

			add_filter( "manage_{$taxonomy}_custom_column", function ( $out, $column_name, $term_id ) use ( $iciav, $taxonomy ) {
				switch ( $column_name ) {
					case 'thumbnail':
						$term = get_term( $term_id );
						do_action( "{$taxonomy}_term_exist", $term );
						$thumbnail_url = $iciav->get_data( 'category_image' ) ?: 'http://www.acsu.buffalo.edu/~rslaine/imageNotFound.jpg';

						$out .= '<img src="' . $thumbnail_url . '" width="100" height="100" />';
						break;
					default:
						break;
				};

				return $out;
			}, 10, 3 );

			if ( ! is_admin() ) {
				add_filter( 'category_description', function ( $description, $category ) {
					$template = new ICIAV_Image_And_Video_Template();

					return $template->render( array(
						'term_id'          => $category,
						'term_description' => $description
					) );
				}, 10, 2 );
			}
		}

		/**
		 * Add multi language support
		 */
		public function multi_language_support() {
			$mo_file_path = INNOCODE_CIAV_PATH . '/languages/';

			load_plugin_textdomain( 'iciav', false, $mo_file_path );
		}

		/**
		 * Installation plugin
		 *
		 * @param $network_wide
		 */
		public function install( $network_wide = false ) {
			add_action( 'plugins_loaded', array( $this, 'multi_language_support' ) );

			// Bail if term meta table is not installed.
			if ( get_option( 'db_version' ) < 34370 ) {
				global $wpdb;

				require __DIR__ . '/capability/autoload.php';
				$taxonomymetatable = ICIAV_Taxonomy_Metatable::get_instance();

				add_action( 'init', array( $taxonomymetatable, 'wpdbfix' ) );
				add_action( 'switch_blog', array( $taxonomymetatable, 'wpdbfix' ) );
				add_action( 'wpmu_new_blog', array( $taxonomymetatable, 'new_blog' ), 10, 6 );

				if ( ! $network_wide ) {
					$taxonomymetatable->create_taxonomy_metatable();

					return;
				}

				$blogs = $wpdb->get_col( "SELECT blog_id FROM {$wpdb->blogs} WHERE site_id = '{$wpdb->siteid}'" );

				foreach ( $blogs as $blog_id ) {
					$taxonomymetatable->create_taxonomy_metatable( $blog_id );

					restore_current_blog();
				}
			}
		}

		/**
		 * Uninstall plugin
		 *
		 * @param $network_wide
		 */
		public function uninstall( $network_wide = false ) {
			// Bail if term meta table is not installed.
			if ( get_option( 'db_version' ) < 34370 ) {
				global $wpdb;

				require __DIR__ . '/capability/autoload.php';
				$taxonomymetatable = ICIAV_Taxonomy_Metatable::get_instance();

				if ( ! $network_wide ) {
					$taxonomymetatable->remove_taxonomy_metatable();

					return;
				}

				$blogs = $wpdb->get_col( "SELECT blog_id FROM {$wpdb->blogs} WHERE site_id = '{$wpdb->siteid}'" );

				foreach ( $blogs as $blog_id ) {
					$taxonomymetatable->remove_taxonomy_metatable( $blog_id );

					restore_current_blog();
				}
			}
		}
	}
}

$plugin = Innocode_Category_Image_And_Video::get_instance();
register_activation_hook( __FILE__, array( $plugin, 'install' ) );
register_deactivation_hook( __FILE__, array( $plugin, 'uninstall' ) );

add_action( 'plugins_loaded', array( $plugin, 'exec' ) );
