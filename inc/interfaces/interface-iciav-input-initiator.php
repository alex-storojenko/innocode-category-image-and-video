<?php

if ( ! interface_exists( 'ICIAV_Input_Initiator' ) ) {
	/**
	 * Contract for getter data
	 *
	 * Interface ICIAV_Input_Initiator
	 *
	 * @package innocode-category-image-and-video
	 */
	interface ICIAV_Input_Initiator {
		/**
		 * Get data from storage by input_id
		 *
		 * @param int $input_id
		 *
		 * @return mixed
		 */
		public function get_data( $input_id );

		/**
		 * Unique key
		 * for processing input
		 *
		 * @return string
		 */
		public function get_key();
	}
}