<?php

if ( ! interface_exists( 'ICIAV_Input' ) ) {
	/**
	 * Contract for input render
	 *
	 * Interface ICIAV_Input
	 *
	 * @package innocode-category-image-and-video
	 */
	interface ICIAV_Input extends ICIAV_Template {
		/**
		 * Get unique id of input
		 *
		 * @return mixed
		 */
		public function get_id();

		/**
		 * Get label of input
		 *
		 * @return mixed
		 */
		public function get_label();

		/**
		 * Get options for input
		 *
		 * @return mixed
		 */
		public function get_options();

		/**
		 * Get value of input
		 *
		 * @return mixed
		 */
		public function get_value();
	}
}