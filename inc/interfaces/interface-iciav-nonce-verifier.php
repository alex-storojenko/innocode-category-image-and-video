<?php

if ( ! interface_exists( 'ICIAV_Nonce_Verifier' ) ) {
	/**
	 * Contract for nonce verify
	 *
	 * interface ICIAV_Nonce_Verifier
	 *
	 * @package innocode-category-image-and-video
	 */
	interface ICIAV_Nonce_Verifier {
		/**
		 * Check nonce protection
		 *
		 * @param string $nonce
		 * @param string $action
		 *
		 * @return bool
		 */
		public function verify_nonce( $nonce, $action );
	}
}