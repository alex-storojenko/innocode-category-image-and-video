<?php

if ( ! interface_exists( 'ICIAV_Acl_Checker' ) ) {
	/**
	 * Contract for check user permissions
	 *
	 * Interface ICIAV_Acl_Checker
	 *
	 * @package innocode-category-image-and-video
	 */
	interface ICIAV_Acl_Checker {
		/**
		 * Check can user do some action
		 *
		 * @param string $action
		 *
		 * @return bool
		 */
		public function user_can( $action );
	}
}