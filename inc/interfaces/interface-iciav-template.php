<?php

if ( ! interface_exists( 'ICIAV_Template' ) ) {
	/**
	 * Contract for view elements render
	 *
	 * Interface ICIAV_Template
	 *
	 * @package innocode-category-image-and-video
	 */
	interface ICIAV_Template {
		/**
		 * render html
		 *
		 * @param array $data
		 *
		 * @return mixed|void
		 */
		public function render( array $data );
	}
}