<?php

/**
 * Source list for autoload files
 *
 * @package innocode-category-image-and-video
 */
require __DIR__ . '/interface-iciav-template.php';
require __DIR__ . '/interface-iciav-input.php';
require __DIR__ . '/interface-iciav-input-initiator.php';
require __DIR__ . '/interface-iciav-nonce-verifier.php';
require __DIR__ . '/interface-iciav-acl-checker.php';
