<?php
if ( ! class_exists( 'ICIAV_Bad_Args_For_Called_Func_Exception' ) ) {
	/**
	 * Exception
	 *
	 * Class ICIAV_Bad_Args_For_Called_Func_Exception
	 *
	 * @package innocode-category-image-and-video
	 */
	class ICIAV_Bad_Args_For_Called_Func_Exception extends Exception {

		/**
		 * ICIAV_Call_Unexpected_Method_Exception constructor
		 *
		 * @param string $message
		 * @param int $code
		 * @param Throwable|null $previous
		 */
		public function __construct( $message = "", $code = 500, Throwable $previous = null ) {
			parent::__construct( $message, $code, $previous );
		}
	}
}