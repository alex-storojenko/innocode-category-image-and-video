<?php
if ( ! class_exists( 'ICIAV_Taxonomy_Metafields' ) ) {
	/**
	 * Metafields framework
	 *
	 * Class ICIAV_Taxonomy_Metafields
	 *
	 * @package innocode-category-image-and-video
	 */
	class ICIAV_Taxonomy_Metafields implements ICIAV_Input_Initiator, ICIAV_Nonce_Verifier, ICIAV_Acl_Checker {
		use ICIAV_Acl_Check_Helper, ICIAV_Nonce_Verify_Helper;

		/**
		 * @input_types:
		 *  image,
		 *  video
		 *
		 * @sanitize_callbacks:
		 *  text_input,
		 *  color_input,
		 *  url_input,
		 *  email_input,
		 *  textarea_input,
		 *  by_pattern_input, //if chosen it you need add sanitize_pattern(Regex value) to array of input
		 *  none_sanitize_input
		 *
		 * <code>
		 * [
		 *      [
		 *          //id is Required. Attention! id must always be unique, otherwise the result will be unexpected.
		 *          'id'                => 'featured_image',
		 *          'label'             => 'Image',
		 *          'type'              => 'image',//@see @input_types
		 *          'options'           => [], //additional options e.g for select [$title => $value]
		 *          'sanitize_callback' => 'url_input' //@see @sanitize_callback
		 *      ],
		 *      [
		 *          'id'                => 'video',
		 *          'label'             => 'Video Youtube/Vimeo',
		 *          'type'              => 'video',
		 *          'description'       => '',
		 *          'options'           => [], //additional options e.g for select [$title => $value]
		 *          'sanitize_callback' => 'url_input'
		 *       ],
		 * ]
		 * </code>
		 *
		 * @var array
		 */
		protected $structure;

		/**
		 * Taxonomy
		 *
		 * @var string
		 */
		protected $taxonomy;

		/**
		 * Unique key for DB save or get meta data
		 *
		 * @var string
		 */
		protected $meta_key;

		/**
		 * Processed object
		 *
		 * @var WP_Term
		 */
		private $term;

		/**
		 * ICIAV_Taxonomy_Metafields constructor.
		 *
		 * @param string $taxonomy
		 * @param array $structure
		 * @param string $meta_key
		 */
		protected function __construct( $taxonomy, array $structure, $meta_key ) {
			$this->taxonomy  = $taxonomy;
			$this->structure = $structure;
			$this->meta_key  = $meta_key;

			add_action( "created_{$this->taxonomy}", function ( $tern_id ) {
				$this->save( $tern_id );
			}, 10, 2 );
			add_action( "edited_{$this->taxonomy}", function ( $tern_id ) {
				$this->save( $tern_id );
			}, 10, 2 );
			add_action( "{$this->taxonomy}_add_form_fields", function ( $taxonomy_slug ) {
				$this->add_fields( $taxonomy_slug );
			} );
			add_action( "{$this->taxonomy}_edit_form", function ( $term ) {
				$this->add_fields( $term );
			} );
			add_action( "{$this->taxonomy}_term_exist", function ( $term ) {
				$this->term = $term;
			} );
		}

		/**
		 * @inheritdoc
		 *
		 * @param int $input_id
		 * @param bool $single
		 *
		 * @return string
		 */
		public function get_data( $input_id, $single = true ) {
			$term_id = null;

			if ( $this->term ) {
				$term_id = $this->term->term_id;
			}

			return ( ! empty( get_term_meta( $term_id, $this->meta_key, $single )[ $input_id ] ) ) ? get_term_meta( $term_id, $this->meta_key, $single )[ $input_id ] : '';
		}

		/**
		 * @inheritdoc
		 *
		 * @return string
		 */
		public function get_key() {
			return $this->meta_key;
		}

		/**
		 * Save term meta
		 *
		 * @param int $term_id
		 *
		 * @return mixed
		 */
		protected function save( $term_id ) {
			// Ignore quick edit
			if ( isset( $_POST['action'] ) && $_POST['action'] == 'inline-save-tax' ) {
				return $term_id;
			}

			$meta = ( empty( $_POST[ $this->meta_key ] ) ? array() : $_POST[ $this->meta_key ] );

			if ( ! $meta || ! $this->user_can( 'edit_posts' ) ) {
				return $term_id;
			}

			if ( $_POST['_wpnonce_add-tag'] ) {
				if ( ! $this->verify_nonce( $_POST['_wpnonce_add-tag'], "add-tag" ) ) {
					return $term_id;
				}
			} else {
				if ( ! $this->verify_nonce( $_POST['_wpnonce'], "update-tag_$term_id" ) ) {
					return $term_id;
				}
			}

			$sanitized_meta = ICIAV_Input_Sanitizer::sanitize( $meta, $this->structure );

			return update_term_meta( $term_id, $this->meta_key, $sanitized_meta );
		}

		/**
		 * Add custom fields fields
		 *
		 * @param WP_Term|string $term
		 *
		 * @return void
		 */
		protected function add_fields( $term ) {
			if ( $term instanceof WP_Term ) {
				do_action( "{$this->taxonomy}_term_exist", $term );
			}

			foreach ( (array) $this->structure as $field ) {
				/** @var ICIAV_Abstract_Input $input */
				$input = ICIAV_Input_Factory::make( $this, $field );

				$input->render();
			}
		}

		/**
		 * Taxonomy metafields factory
		 *
		 * @param $taxonomy @see ICIAV_Taxonomy_Metafields::$taxonomy
		 * @param array $structure @see ICIAV_Taxonomy_Metafields::$structure
		 * @param string $meta_key @see ICIAV_Taxonomy_Metafields::$meta_key
		 *
		 * @return static
		 */
		public static function create( $taxonomy, array $structure, $meta_key = '_iciav_term_meta' ) {
			return new static( $taxonomy, $structure, $meta_key );
		}
	}
}