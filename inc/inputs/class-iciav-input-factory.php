<?php

if ( ! class_exists( 'ICIAV_Input_Factory' ) ) {
	/**
	 * Input maker
	 *
	 * Class ICIAV_Input_Factory
	 *
	 * @package innocode-category-image-and-video
	 */
	class ICIAV_Input_Factory {
		/**
		 * Input factory
		 *
		 * @param ICIAV_Input_Initiator $initiator
		 * @param array $input_structure
		 *
		 * @return ICIAV_Input
		 * @throws ICIAV_Call_Unexpected_Method_Exception
		 */
		public static function make( ICIAV_Input_Initiator $initiator, array $input_structure ) {
			$type = ( empty( $input_structure['type'] ) ) ? 'text' : $input_structure['type'];

			$id      = $input_structure['id'];
			$label   = ( ! empty( $input_structure['label'] ) ) ? $input_structure['label'] : '';
			$options = ( ! empty( $input_structure['options'] ) ? $input_structure['options'] : array() );

			$name_of_input_class = 'ICIAV_Input_' . ucfirst( $type );

			if ( ! class_exists( $name_of_input_class ) ||
			     ! in_array( 'ICIAV_Input', class_implements( $name_of_input_class ) )
			) {
				throw new ICIAV_Call_Unexpected_Method_Exception( 'Method not exists. You should add needed input class' );
			}

			return new $name_of_input_class( $initiator, $id, $label, $options );
		}
	}
}