<?php

if ( ! class_exists( 'ICIAV_Input_Video' ) ) {
	/**
	 * Input include call media lib
	 *
	 * Class ICIAV_Input_video
	 *
	 * @package innocode-category-video-and-video
	 */
	class ICIAV_Input_Video extends ICIAV_Abstract_Input {
		/**
         * @inheritdoc
         *
		 * ICIAV_Input_Video constructor
		 *
		 * @param ICIAV_Input_Initiator $initiator
		 * @param $id
		 * @param $label
		 * @param array $options
		 */
	    public function __construct( ICIAV_Input_Initiator $initiator, $id, $label, array $options ) {
		    parent::__construct( $initiator, $id, $label, $options );

		    /**
		     * Since version 3.3
		     * Wp_enqueue_script() can be called at the time the page is generated.
		     * In this case, the called script will be connected in the basement,
		     * at the moment when wp_footer events are triggered.
		     *
		     * It is necessary for dynamic connection of scripts and styles
		     * during the generation of input
		     */
		    $this->enqueue_assets();
	    }

		/**
		 * @inheritdoc
		 *
		 * @param array $data
		 */
		public function render( array $data = array() ) {
			$value = $this->get_value();
			?>
			<div class="iciav form-field <?= ICIAV_Str_Helper::str_snake_to_kebab( get_class( $this->initiator ) ); ?> input-area input-area-video input-area-video-<?= $this->id . ' ' . ICIAV_Str_Helper::str_snake_to_kebab( get_class( $this ) ); ?>">
				<label class="iciav input-label label-video label-video-<?= $this->id . ' ' . ICIAV_Str_Helper::str_snake_to_kebab( get_class( $this ) ); ?>">
					<input class="iciav input input-text input-text-<?= $this->id . ' ' . ICIAV_Str_Helper::str_snake_to_kebab( get_class( $this ) ); ?>"
					       type="text" name="<?= $this->initiator->get_key(); ?>[<?= $this->id ?>]"
					       id="<?= $this->id ?>" value="<?= $value ?>"/>
					<span class="iciav label-title label-title-video label-title-video-<?= $this->id . ' ' . ICIAV_Str_Helper::str_snake_to_kebab( get_class( $this ) ); ?>"><?= $this->label; ?></span>
				</label>
				<div class="iciav button-group">
					<button class="iciav button remove-video"><?= __( 'Clear', 'iciav' ) ?></button>
					<button class="iciav button preview-video" <?= ( ! $value ) ? 'disabled' : ''; ?>><?= __( 'Preview', 'iciav' ) ?></button>
				</div>
				<div id="popup-<?= $this->id; ?>" class="iciav popup-overlay hidden">
					<div class="iciav popup-preview-video">
						<span class="iciav popup-preview-video-title">Video preview</span>
						<iframe class="iciav preview-video" src="<?= $this->get_value() ?>" frameborder="0" allowfullscreen width="560px" height="415px"></iframe>
                        <a class="close" href="javascript:;">&times;</a>
					</div>
				</div>
			</div>
			<?php
		}

		/** Enqueue special assets */
		protected function enqueue_assets() {
			wp_enqueue_script( 'iciav-init-video', INNOCODE_CIAV_URI . 'inc/inputs/assets/js/init.video.js', array( 'jquery' ), INNOCODE_CIAV_VERSION, true );
		}
	}
}