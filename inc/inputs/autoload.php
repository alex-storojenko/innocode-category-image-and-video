<?php

/**
 * Source list for autoload files
 *
 * @package innocode-category-image-and-video
 */
require __DIR__ . '/class-iciav-abstract-input.php';
require __DIR__ . '/class-iciav-input-sanitizer.php';
require __DIR__ . '/class-iciav-input-image.php';
require __DIR__ . '/class-iciav-input-video.php';
require __DIR__ . '/class-iciav-input-factory.php';