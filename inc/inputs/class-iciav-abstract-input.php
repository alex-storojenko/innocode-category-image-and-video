<?php

if ( ! class_exists( 'ICIAV_Abstract_Input' ) ) {
	/**
	 * Common logic of inputs
	 *
	 * class ICIAV_Abstract_Input
	 *
	 * @package innocode-category-image-and-video
	 */
	abstract class ICIAV_Abstract_Input implements ICIAV_Input {
		/**
		 * Class input caller
		 *
		 * @var ICIAV_Input_Initiator $initiator
		 */
		protected $initiator;

		/**
		 * Uniq Id for input
		 *
		 * @var string $id
		 */
		protected $id;

		/**
		 * Input label
		 *
		 * @var string $label
		 */
		protected $label;

		/**
		 * Array of options for render select type inputs
		 *
		 * @var array
		 */
		protected $options;

		/**
		 * ICIAV_Abstract_Input constructor
		 *
		 * @param ICIAV_Input_Initiator $initiator
		 * @param $id
		 * @param $label
		 * @param array $options
		 */
		public function __construct( ICIAV_Input_Initiator $initiator, $id, $label, array $options ) {
			$this->initiator = $initiator;
			$this->id        = $id;
			$this->label     = $label;
			$this->options   = $options;

			/**
			 * Since version 3.3
			 * Wp_enqueue_script() can be called at the time the page is generated.
			 * In this case, the called script will be connected in the basement,
			 * at the moment when wp_footer events are triggered.
			 *
			 * It is necessary for dynamic connection of scripts and styles
			 * during the generation of input
			 */
			$this->enqueue_main_assets();
		}

		/**
		 * Get Id
		 *
		 * @return string
		 */
		public function get_id() {
			return $this->id;
		}

		/**
		 * Get label
		 *
		 * @return string
		 */
		public function get_label() {
			return $this->label;
		}

		/**
		 * Get options
		 *
		 * @return array
		 */
		public function get_options() {
			return $this->options;
		}

		/**
		 * @inheritdoc
		 *
		 * Rewrite in use classes
		 */
		abstract public function render( array $data = array() );

		/**
		 * Getting value
		 *
		 * @return string
		 */
		public function get_value() {
			return $this->initiator->get_data( $this->id );
		}

		/** Enqueue assets */
		protected function enqueue_main_assets() {
			wp_enqueue_style( 'iciav-inputs-style', INNOCODE_CIAV_URI . 'inc/inputs/assets/css/style.css', array(), INNOCODE_CIAV_VERSION );
		}
	}
}