<?php

if ( ! class_exists( 'ICIAV_Input_Image' ) ) {
	/**
	 * Input include call media lib
	 *
	 * Class ICIAV_Input_Image
	 *
	 * @package innocode-category-image-and-video
	 */
	class ICIAV_Input_Image extends ICIAV_Abstract_Input {
		/**
		 * @inheritdoc
         *
		 * ICIAV_Input_Image constructor
		 *
		 * @param ICIAV_Input_Initiator $initiator
		 * @param $id
		 * @param $label
		 * @param array $options
		 */
		public function __construct( ICIAV_Input_Initiator $initiator, $id, $label, array $options ) {
			parent::__construct( $initiator, $id, $label, $options );

			/**
			 * Since version 3.3
			 * Wp_enqueue_script() can be called at the time the page is generated.
			 * In this case, the called script will be connected in the basement,
			 * at the moment when wp_footer events are triggered.
			 *
			 * It is necessary for dynamic connection of scripts and styles
			 * during the generation of input
			 */
			$this->enqueue_assets();
		}

		/**
		 * @inheritdoc
		 *
		 * @param array $data
		 */
		public function render( array $data = array() ) {
			$value = $this->get_value();
			?>
            <div class="iciav form-field <?= ICIAV_Str_Helper::str_snake_to_kebab( get_class( $this->initiator ) ); ?> input-area input-area-image input-area-image-<?= $this->id . ' ' . ICIAV_Str_Helper::str_snake_to_kebab( get_class( $this ) ); ?>">
                <label class="iciav input-label label-image label-image-<?= $this->id . ' ' . ICIAV_Str_Helper::str_snake_to_kebab( get_class( $this ) ); ?>">
                    <input class="iciav input input-text input-text-<?= $this->id . ' ' . ICIAV_Str_Helper::str_snake_to_kebab( get_class( $this ) ); ?>"
                           type="text" name="<?= $this->initiator->get_key(); ?>[<?= $this->id ?>]"
                           id="<?= $this->id ?>" value="<?= $value ?>"/>
                    <span class="iciav label-title label-title-image label-title-image-<?= $this->id . ' ' . ICIAV_Str_Helper::str_snake_to_kebab( get_class( $this ) ); ?>"><?= $this->label; ?></span>
                </label>
                <div class="iciav button-group">
                    <button class="iciav button call-media"><?= __( 'Select', 'iciav' ) ?></button>
                    <button class="iciav button remove-image"><?= __( 'Clear', 'iciav' ) ?></button>
                    <button class="iciav button preview-image" <?= ( ! $value ) ? 'disabled' : ''; ?>><?= __( 'Preview', 'iciav' ) ?></button>
                </div>
                <div id="popup-<?= $this->id; ?>" class="iciav popup-overlay hidden">
                    <div class="iciav popup-preview-image">
                        <span class="iciav popup-preview-image-title">Image preview</span>
                        <img class="iciav image" src="<?= $value ?: '#' ?>"/>
                        <a class="close" href="javascript:;">&times;</a>
                    </div>
                </div>
            </div>
			<?php
		}

		/** Enqueue special assets */
		protected function enqueue_assets() {
			if ( ! did_action( 'wp_enqueue_media' ) ) {
				wp_enqueue_media();
			}

			wp_enqueue_script( 'iciav-init-uploader', INNOCODE_CIAV_URI . 'inc/inputs/assets/js/init.uploader.js', array( 'jquery' ), INNOCODE_CIAV_VERSION, true );
		}
	}
}