(function ($) {
    'use strict';

    var $wrapper = $('.iciav.input-area-video');

    var parseVideo = function (url) {
        // - Supported YouTube URL formats:
        //   - http://www.youtube.com/watch?v=My2FRPA3Gf8
        //   - http://youtu.be/My2FRPA3Gf8
        //   - https://youtube.googleapis.com/v/My2FRPA3Gf8
        // - Supported Vimeo URL formats:
        //   - http://vimeo.com/25451551
        //   - http://player.vimeo.com/video/25451551
        // - Also supports relative URLs:
        //   - //player.vimeo.com/video/25451551

        url.match(/(http:|https:|)\/\/(player.|www.)?(vimeo\.com|youtu(be\.com|\.be|be\.googleapis\.com))\/(video\/|embed\/|watch\?v=|v\/)?([A-Za-z0-9._%-]*)(\&\S+)?/);

        var type,
            provider = RegExp.$3,
            id = RegExp.$6;

        if (provider.indexOf('youtu') > -1) {
            type = 'youtube';
        } else if (provider.indexOf('vimeo') > -1) {
            type = 'vimeo';
        }

        return {
            type: type,
            id: id
        };
    };

    $(document).ready(function () {
        var togglebyClass = function (htmlElement) {
            if (htmlElement.hasClass('hidden')) {
                htmlElement.removeClass('hidden');
                htmlElement.addClass('visible');
            } else {
                htmlElement.removeClass('visible');
                htmlElement.addClass('hidden');
            }
        };

        $wrapper.on('change keyup blur', 'input.iciav-input-video', function (e) {
            e.preventDefault();

            var url = $(this).val();
            var $localWrapper = $(this).parent().parent();
            var $previewPopup = $localWrapper.find('.iciav.popup-overlay');
            var $previewButton = $localWrapper.find('.button-group > .preview-video');
            var $previewIframe = $previewPopup.find('iframe.preview-video');
            var videoObj = parseVideo(url);

            if (videoObj.type === 'youtube') {
                $previewIframe.attr('src', '//www.youtube.com/embed/' + videoObj.id);
                $(this).val('//www.youtube.com/embed/' + videoObj.id);
            } else if (videoObj.type === 'vimeo') {
                $previewIframe.attr('src', '//player.vimeo.com/video/' + videoObj.id);
                $(this).val('//player.vimeo.com/video/' + videoObj.id);
            } else {
                $previewButton.attr('disabled', 'disabled');

                return;
            }

            $previewButton.removeAttr('disabled', 'disabled');
        });

        $wrapper.on('click', '.button-group > .remove-video', function (e) {
            e.preventDefault();

            var $localWrapper = $(this).parent().parent();
            var $previewPopup = $localWrapper.find('.iciav.popup-overlay');
            var $previewButton = $localWrapper.find('.button-group > .preview-video');
            var $urlInput = $localWrapper.find('.input-text');
            var $previewIframe = $previewPopup.find('iframe.preview-video');

            if (!$previewPopup.hasClass('hidden')) {
                togglebyClass($previewPopup);
            }

            $previewButton.attr('disabled', 'disabled');
            $previewIframe.attr('src', '');
            $urlInput.val('');
        });

        $wrapper.on('click', '.button-group > .preview-video', function (e) {
            e.preventDefault();

            var $localWrapper = $(this).parent().parent();
            var $previewPopup = $localWrapper.find('.iciav.popup-overlay');

            togglebyClass($previewPopup);
        });

        $wrapper.on('click', '.popup-preview-video > .close', function (e) {
            e.preventDefault();

            var $previewPopup = $(this).closest('.iciav.popup-overlay');

            togglebyClass($previewPopup);
        });
    });
})(jQuery);