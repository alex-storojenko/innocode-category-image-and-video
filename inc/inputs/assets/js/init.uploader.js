(function ($) {
    'use strict';

    var $wrapper = $('.iciav.input-area-image');

    $(document).ready(function () {
        var togglebyClass = function (htmlElement) {
            if (htmlElement.hasClass('hidden')) {
                htmlElement.removeClass('hidden');
                htmlElement.addClass('visible');
            } else {
                htmlElement.removeClass('visible');
                htmlElement.addClass('hidden');
            }
        };

        $wrapper.on('click', '.button-group > .call-media', function (e) {
            e.preventDefault();

            var $localWrapper = $(this).parent().parent();
            var $previewPopup = $localWrapper.find('.iciav.popup-overlay');
            var $previewButton = $localWrapper.find('.button-group > .preview-image');
            var $previewImg = $previewPopup.children().children();
            var $urlInput = $localWrapper.find('.input-text');
            var sendAttachmentBkp = wp.media.editor.send.attachment;

            wp.media.editor.send.attachment = function (props, attachment) {
                $previewButton.removeAttr('disabled', 'disabled');
                $previewImg.attr('src', attachment.url);
                $urlInput.val(attachment.url);

                if (!$previewPopup.hasClass('visible')) {
                    togglebyClass($previewPopup);
                }

                wp.media.editor.send.attachment = sendAttachmentBkp;
            };

            wp.media.editor.open();
        });

        $wrapper.on('click', '.button-group > .remove-image', function (e) {
            e.preventDefault();

            var $localWrapper = $(this).parent().parent();
            var $previewPopup = $localWrapper.find('.iciav.popup-overlay');
            var $previewButton = $localWrapper.find('.button-group > .preview-image');
            var $urlInput = $localWrapper.find('.input-text');
            var $previewImg = $previewPopup.children().children();

            if (!$previewPopup.hasClass('hidden')) {
                togglebyClass($previewPopup);
            }

            $previewButton.attr('disabled', 'disabled');
            $previewImg.attr('src', '#');
            $urlInput.val('');
        });

        $wrapper.on('click', '.button-group > .preview-image', function (e) {
            e.preventDefault();

            var $localWrapper = $(this).parent().parent();
            var $previewPopup = $localWrapper.find('.iciav.popup-overlay');

            togglebyClass($previewPopup);
        });

        $wrapper.on('click', '.popup-preview-image > .close', function (e) {
            e.preventDefault();

            var $previewPopup = $(this).closest('.iciav.popup-overlay');

            togglebyClass($previewPopup);
        });
    });
})(jQuery);