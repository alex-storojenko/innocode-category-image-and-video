<?php

/**
 * Source list for autoload files
 *
 * @package innocode-category-image-and-video
 */
require __DIR__ . '/interfaces/autoload.php';
require __DIR__ . '/exceptions/autoload.php';
require __DIR__ . '/helpers/autoload.php';
require __DIR__ . '/inputs/autoload.php';
require __DIR__ . '/metafields/autoload.php';
require __DIR__ . '/templates/autoload.php';