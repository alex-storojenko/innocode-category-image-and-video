<?php

if ( ! trait_exists( 'ICIAV_Nonce_Verify_Helper' ) ) {
	/**
	 * Nonce verify wrapper
	 *
	 * Trait ICIAV_Nonce_Verify_Helper
	 *
	 * @package innocode-category-image-and-video
	 */
	trait ICIAV_Nonce_Verify_Helper {
		/**
		 * Check nonce protection
		 *
		 * @param $nonce
		 * @param $action
		 *
		 * @return bool
		 */
		public function verify_nonce( $nonce, $action ) {
			return wp_verify_nonce( $nonce, $action );
		}
	}
}