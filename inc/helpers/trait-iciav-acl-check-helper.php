<?php

if ( ! trait_exists( 'ICIAV_Acl_Check_Helper' ) ) {
	/**
	 * Nonce verify wrapper
	 *
	 * Trait ICIAV_Acl_Check_Helper
	 *
	 * @package innocode-category-image-and-video
	 */
	trait ICIAV_Acl_Check_Helper {
		/**
		 * Check can user do some action
		 *
		 * @param string $action
		 *
		 * @return bool
		 */
		public function user_can( $action ) {
			return current_user_can( $action );
		}
	}
}