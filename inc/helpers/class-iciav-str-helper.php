<?php

if ( ! class_exists( 'ICIAV_Str_Helper' ) ) {
	/**
	 * String helper
	 *
	 * Class ICIAV_Str_Helper
	 *
	 * @package innocode-category-image-and-video
	 */
	class ICIAV_Str_Helper {
		/**
		 * Convert snake to kebab
		 *
		 * @param $snake_str
		 *
		 * @return string
		 */
		public static function str_snake_to_kebab( $snake_str ) {
			return mb_strtolower( str_replace( '_', '-', trim( $snake_str ) ), 'UTF-8' );
		}

		/**
		 * Convert snake to camel
		 *
		 * @param $snake_str
		 *
		 * @return mixed
		 */
		public static function str_snake_to_camel( $snake_str ) {
			$arr = explode( '_', mb_strtolower( $snake_str, 'UTF-8' ) );

			for ( $i = 0, $c = count( $arr ); $i < $c; $i ++ ) {
				if ( $i > 0 ) {
					$arr[ $i ] = ucfirst( $arr[ $i ] );
				}
			}

			return implode( '', $arr );
		}
	}
}