<?php

if ( ! class_exists( 'ICIAV_Image_And_Video_Render' ) ) {
	/**
	 * Render image and video from category
	 *
	 * Class ICIAV_Image_And_Video_Render
	 *
	 * @package innocode-category-image-and-video
	 */
	class ICIAV_Image_And_Video_Template implements ICIAV_Template {
		/**
		 * @inheritdoc
		 *
		 * @param array $data
		 */
		public function render( array $data ) {
			ob_start();

			$term_id          = ( ! empty( $data['term_id'] ) ) ? $data['term_id'] : null;
			$term_description = ( ! empty( $data['term_description'] ) ) ? $data['term_description'] : '';
			$term_meta        = maybe_unserialize( get_term_meta( $term_id, '_iciav_term_meta', true ) );
			$image_url        = ( ! empty( $term_meta['category_image'] ) ) ? $term_meta['category_image'] : '';
			$video_url        = ( ! empty( $term_meta['category_video'] ) ) ? $term_meta['category_video'] : '';
			?>
            <div class="iciav iciav-category-image-area">
                <img class="iciav iciav-category-image" src="<?= $image_url ?>" alt="..."/>
            </div>
            <div class="iciav iciav-category-video-area">
                <iframe class="iciav iciav-category-video" src="<?= $video_url ?>" frameborder="0" allowfullscreen
                        width="516px" height="350px"></iframe>
            </div>
            <div class="iciav iciav-category-description-area">
                <p class="iciav iciav-category-description"><?= $term_description; ?></p>
            </div>
			<?php

			return ob_get_clean();
		}
	}
}